package dao;

import com.lagou.RunBoot;
import com.lagou.entity.COrder;
import com.lagou.repository.COrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RunBoot.class)
public class TestShardingDatabase {

    @Autowired
    private COrderRepository orderRepository;

    @Test
    public void insert(){
        List<COrder> orderList = new ArrayList<>();
        for (int i=1;i<=20;i++){
            COrder order = new COrder();
            Random random = new Random();
            int userId = random.nextInt(10);
            int companyId = random.nextInt(10);
//            position.setId(i);
            order.setIsDel(false);
            order.setCompanyId(companyId);
            order.setPositionId(3242342);
            order.setUserId(userId);
            order.setPublishUserId(1111);
            order.setResumeType(1);
            order.setStatus("AUTO");
            order.setCreateTime(new Date());
            order.setUpdateTime(new Date());
            orderList.add(order);
        }
        orderRepository.saveAll(orderList);
    }

    @Test
    public void select(){
        Optional<COrder> order = orderRepository.findById(547135372363038720L);
        System.out.println(order.get().getCompanyId());
    }

}
