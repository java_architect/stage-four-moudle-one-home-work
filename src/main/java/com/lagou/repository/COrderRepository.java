package com.lagou.repository;

import com.lagou.entity.COrder;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author kim
 * @date 2020/12/19 下午1:05
 * @description
 */
public interface COrderRepository extends JpaRepository<COrder, Long> {
}
