基于Docker、MySQL8 Group Replication

测试代码在[TestShardingDatabase.java](src/test/java/dao/TestShardingDatabase.java)，包含插入和查询

演示视频地址：https://www.bilibili.com/video/BV1CT4y1M7wg/

备用演示视频地址：链接: https://pan.baidu.com/s/11cUJpmObevteGay6POLzKg  密码: 81va

分库分表和主从规则，具体配置见[application-sharding-master-slaves.properties](src/main/resources/application-sharding-master-slaves.properties)：

```shell
masterSlaveRules:
  master0:
    masterDataSourceName: master0
    name: master0
    slaveDataSourceNames:
    - slave0
    - slave1
  master1:
    masterDataSourceName: master1
    name: master1
    slaveDataSourceNames:
    - slave2
    - slave3
tables:
  c_order:
    actualDataNodes: master$->{0..1}.c_order$->{0..1}
    databaseStrategy:
      inline:
        algorithmExpression: master$->{user_id % 2}
        shardingColumn: user_id
    keyGenerator:
      column: id
      type: SNOWFLAKE
    logicTable: c_order
    tableStrategy:
      inline:
        algorithmExpression: c_order$->{id % 2}
        shardingColumn: id
```